# Minecraft Base Project

IntelliJ base project related to Minecraft Lab of 221_SIn & 231_SIn course

# Authors 
	Gautier Dorsaz      gautier.dorsaz@students.hevs.ch
	Maxime  Selles      maxime.selles@students.hevs.ch

 # Objectifs

    Electrical network Management of a Minecraft Electrical Age city

 

# Introduction:

 

The classes `DataPoint`, `FloatDataPoint`, and `BooleanDataPoint` are part of a larger system designed to interface the Minecraft game world with external systems such as web applications or databases.

 

`DataPoint` is an abstract class that serves as a base for all types of data points that can be exchanged between Minecraft and the external systems. It has two main attributes, a `label` that identifies the data point and a boolean `isOutput` flag that specifies whether it is an output (control) or input (measurement) data point.

 

`FloatDataPoint` and `BooleanDataPoint` are two subclasses of `DataPoint` that represent specific types of data points. `FloatDataPoint` stores a `float` value, while `BooleanDataPoint` stores a `boolean` value. Both of them implement the `getValue()` method, which returns the stored value as a string.

 

All data points have a `pushValueToConnectors()` method that is called whenever the value of the data point changes. This method updates all the connectors of the system with the new value of the data point. Connectors are used to exchange data between the Minecraft game world and external systems. There are three types of connectors implemented in the system: `DatabaseConnector`, `WebConnector`, and `FieldConnector`.

 

Finally, the `DataPointListener` interface is implemented by classes that need to listen for changes in data points. This interface has only one method, `onNewValue()`, which is called whenever a data point is updated.

 

The system works by creating instances of `DataPoint` subclasses with specific labels and flags indicating whether they are inputs or outputs. Then, whenever the value of a data point changes, the `pushValueToConnectors()` method is called, which updates the connectors with the new value of the data point. External systems can then listen for changes in specific data points by implementing the `DataPointListener` interface and registering themselves with the appropriate connectors.

 

The `SmartController` class is the brain of the system, responsible for controlling the behavior of the Minecraft world based on the inputs it receives from the external world through the `DataPoint` instances. It receives inputs from the `DatabaseConnector`, the `WebConnector`, and the `FieldConnector`. 

 

The `SmartController` class contains a set of `Rule` instances that define the logic for how the inputs should be interpreted and what outputs should be generated. The `Rule` class contains a set of conditions, which are defined by `DataPoint` instances, and a set of actions to take if those conditions are met.

 

Overall, the `SmartController` acts as the bridge between the external world and the Minecraft world, translating inputs into actions that affect the game world.

 

 

# How to launch:

Start the Minecraft World first

For launch the world, in your terminal go on “ElectricalAge-develop” and then enter the next command:  .\gradlew runClient,

For launch MinecraftController, in your terminal enter the next command: `java -jar Minecraft.jar https://influx.sdi.hevs.ch SIn03 SIn03 localhost 1502 -eraseDB`
This structure of message is not the same has asked in Cyberlearn, but respect the code given at the beginning of the project.

# Links
[JavaDoc](javadoc/hei/index.html)

[JarFile](out/artifacts/Minecraft_jar/Minecraft.jar)

[CSVFile](src/main/resources/ModbusMap.csv)

[DataBase](https://grafana.sdi.hevs.ch/)

[InfluxDB](https://influx.sdi.hevs.ch/orgs/5de694672d3f2464)

[WebConnector](src/main/resources/WebClient/index.html)

# Specification
-Java 1.8

-Minecraft Electrical Age


# Used Libraries

-Modbus4j

-Based on the code of @PatriceRudaz



