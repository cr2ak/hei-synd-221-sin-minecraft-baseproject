package ch.hevs.isi;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.CSVReader;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.field.ModbusAccessor;
import ch.hevs.isi.smartcontroler.SmartControl;
import ch.hevs.isi.utils.Utility;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class MinecraftController {

    public static boolean ERASE_PREVIOUS_DATA_INB_DB        = false;

    public static void usage() {
        System.out.println();
        System.out.println("You're reading this message because no parameter (or not the needed ones) has been passed to the application.");
        System.out.println();
        System.out.println("In development mode, just add to your running configuration the needed parameters (see usage below).");
        System.out.println("In running mode, the application's usage is the following:");
        System.out.println("java MinecraftController <InfluxDB Server> <DB Name> <DB Measurement> <DB Username> <ModbusTCP Server> <ModbusTCP port> [-modbus4j] [-keepAlive]");
        System.out.println("where:");
        System.out.println("- <InfluxDB Server>:  The complete URL of the InfluxDB server, including the protocol (http or https)...");
        System.out.println("                      Example: https://influx.sdi.hevs.ch");
        System.out.println("- <DB Name>:          The name of the Influx DB to use. For this project, this name is the name of the group you've been affected to. (SInXX)");
        System.out.println("- <DB Username:       The user's name to use to access the DB. It's also your group's name. (SInXX)");
        System.out.println("- <ModbusTCP Server>: The IP address of the Minecraft ModbusTCP server (default value: localhost)");
        System.out.println("- <ModbusTCP port>:   The port number of the Minecraft ModbusTCP server (default value: 1502)");
        System.out.println("- [-eraseDB]:         Optional parameter! If set, the application will erase the previous data in InfluxDB...");
        System.out.println();
        System.exit(1);
    }

    @SuppressWarnings("all")
    public static void main(String[] args) throws MalformedURLException {

        // ------------------------------------- DO NOT CHANGE THE FOLLOWING LINES -------------------------------------
        String dbProtocol       = "http";
        String dbHostName       = "localhost";
        String dbName           = "labo";
        String dbUserName       = "root";
        String dbPassword       = "root";

        String modbusTcpHost    = "localhost";
        int modbusTcpPort       = 1502;

        // Check the number of arguments and show usage message if the number does not match.
        String[] parameters = null;

        // If there is only one number given as parameter, construct the parameters according the group number.
        if (args.length >= 5) {
            parameters = args;

            // Decode parameters for influxDB
            String[] dbParams = parameters[0].split("://");
            if (dbParams.length != 2) {
                usage();
            }

            dbProtocol    = dbParams[0];
            dbHostName    = dbParams[1];
            dbName        = parameters[1];
            dbUserName    = parameters[2];
            dbPassword    = Utility.md5sum(dbUserName);

            // Decode parameters for Modbus TCP
            modbusTcpHost = parameters[3];
            modbusTcpPort = Integer.parseInt(parameters[4]);

            for (int i = 5; i < args.length; i++) {
                if (parameters[i].compareToIgnoreCase("-erasedb") == 0) {
                    ERASE_PREVIOUS_DATA_INB_DB = true;
                }
            }
        } else {
            usage();
        }

        // ------------------------------------ /DO NOT CHANGE THE FOLLOWING LINES -------------------------------------

        // Start coding here ...
        System.out.println("Author @Gautier Dorsaz, @Maxime Selles");
        CSVReader.assignDataPoint();
        if(ERASE_PREVIOUS_DATA_INB_DB){
            String actualDate = null ;
            Date.from(Instant.now());
            LocalDateTime now = LocalDateTime.of(LocalDate.now(),LocalTime.now()).minusNanos(LocalTime.now().getNano());
             actualDate =  now.format(DateTimeFormatter.ISO_DATE_TIME)+"Z";
            DatabaseConnector.getInstance().clearDataBase(dbProtocol+"://"+dbHostName,dbName,dbUserName,"Xk6j-2xWvycUc2dvFJNnYn649Dfa_QH95vwDrSEv4noGhmcbVlLCZHo4nNNeU_b_4g2rQJ9fxXVgfCC37hZJSg==",actualDate);
        }else{
            DatabaseConnector.getInstance().configure(dbProtocol+"://"+dbHostName,dbName,dbUserName,"Xk6j-2xWvycUc2dvFJNnYn649Dfa_QH95vwDrSEv4noGhmcbVlLCZHo4nNNeU_b_4g2rQJ9fxXVgfCC37hZJSg==");
        }
        ModbusAccessor.getInstance().connect(modbusTcpHost,modbusTcpPort);
        System.out.println("The jar is running");
        FieldConnector.getInstance().pollTask(2500);
        SmartControl.timerTask(2500);
        }

    }

