package ch.hevs.isi.smartcontroler;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.db.DatabaseConnector;

import java.util.Timer;
import java.util.TimerTask;
/**

 Abstract class that represents a smart control system for energy production and consumption. It provides various static datapoints that are used in the regulation of energy production and consumption. The class contains a private regulation method that implements a complex set of conditions to manage energy production and consumption in real-time based on the values of the datapoints.
 The following static datapoints are available:
 <ul>
 <li>EnergieB: represents the battery charge float.</li>
 <li>ConsoH: represents the home float consumption.</li>
 <li>ConsoP: represents the public float consumption.</li>
 <li>ConsoB: represents the bunker float consumption.</li>
 <li>PuissanceS: represents the solar float power.</li>
 <li>PuissanceW: represents the wind float power.</li>
 <li>PuissanceCF: represents the coal float power.</li>
 <li>PuissanceF: represents the factory float power.</li>
 <li>time: represents the clock float.</li>
 <li>ordrePcf: represents the remote coal setpoint.</li>
 <li>ordrePf: represents the remote factory setpoint.</li>
 <li>ordreS: represents the remote solar switch.</li>
 <li>connectS: represents the solar connection state.</li>
 </ul>

 */
public abstract class SmartControl {

    //chaque fois que la class est appelée on va chercher tous les datapoints utiles
   static DataPoint EnergieB = DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");

    static DataPoint ConsoH = DataPoint.getDataPointFromLabel("HOME_P_FLOAT");

    static DataPoint ConsoP = DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");

    static DataPoint ConsoB = DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");

    static DataPoint PuissanceS = DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");

    static DataPoint PuissanceW = DataPoint.getDataPointFromLabel("WIND_P_FLOAT");

    static DataPoint PuissanceCF = DataPoint.getDataPointFromLabel("COAL_P_FLOAT");

    static DataPoint PuissanceF = DataPoint.getDataPointFromLabel("FACTORY_P_FLOAT");

    static DataPoint time = DataPoint.getDataPointFromLabel("CLOCK_FLOAT");

    static DataPoint ordrePcf = DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");

    static DataPoint ordrePf = DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");

    static DataPoint ordreS = DataPoint.getDataPointFromLabel("REMOTE_SOLAR_SW");

    static DataPoint connectS = DataPoint.getDataPointFromLabel("SOLAR_CONNECT_ST");

    /**
     *  The regulation method implements a complex set of conditions to manage energy production and consumption in real-time based on the values of the datapoints. The method takes the following actions:
     *  <ul>
     *  <li>Check if it is the last day and if it is, the battery charge is maintained at a level above 50%.</li>
     *  <li>If energy production is too high and the battery charge level is above 90%, the solar panel is turned off.</li>
     *  <li>If energy production is too high and the battery charge level is below 90% and the solar panel is off, the solar panel is turned on.</li>
     *  <li>If energy production is too low and the battery charge level is between 70% and 50%, the factory power is reduced.</li>
     *  <li>If the battery charge level is critically low and the energy production is greater than the sum of home, public, and bunker consumption, the coal power is used to charge the battery.</li>
     *  <li>If the battery charge level is between 45% and 25% and the energy production is less than or equal to 666, the factory and coal power are adjusted to a lower value.</li>
     *  <li>If the battery charge level is between 55% and 45% and the energy production is less than or equal to 1000, the factory and coal power are adjusted to a moderate value.</li>
     *  <li>If the energy consumption is greater than the energy production and the battery charge level is less than 40%, the coal power is adjusted based on the difference between consumption and production.</li>
     *  <li>In all other cases, the factory power is set to maximum and the coal power is set to minimum.</li>
     *  </ul>
     *  */

    private static void regulation() {
        //on a tous les datapoints dans la class maintenant on va chercher leurs valeurs
        float Eb = Float.valueOf(EnergieB.getValue());
        float Pw = Float.valueOf(PuissanceW.getValue());
        float Ps = Float.valueOf(PuissanceS.getValue());
        float Pcf = Float.valueOf(PuissanceCF.getValue());
        float Pf = Float.valueOf(PuissanceF.getValue());
        float Ph = Float.valueOf(ConsoH.getValue());
        float Pp = Float.valueOf(ConsoP.getValue());
        float Pb = Float.valueOf(ConsoB.getValue());

        boolean SolarOnOff = ((BooleanDataPoint)connectS).getBooleanValue();


        if (!DatabaseConnector.getInstance().get_timeManager().isLastDay()) {

            if (Pw + Ps > 1000 && Eb > 0.9) {
                System.out.println("solar panel off");
                ordreS.setValue(false);
                ordrePf.setValue(Pw/1000);
            }
            else if (Pw + Ps > 1000 && Eb < 0.9 &&  !SolarOnOff) {

                ordreS.setValue(true);
                ordrePf.setValue(1);
            }
            else if (Pw + Ps < 1000 && Eb < 0.7 && Eb >= 0.5) {
                ordrePf.setValue(0.7f);
            }
            else if (Eb < 0.25 && Ph+Pb+Pp<Pw+Ps) {

                ordrePf.setValue(0);
                ordrePcf.setValue(0.25f);
            }
            else if (Eb < 0.45 && Eb > 0.25) {
                if (Pw + Ps <= 666) {

                    ordrePf.setValue(0.3f);
                    ordrePcf.setValue(0.15f);
                }
                else {
                    ordrePf.setValue(0.5f);
                    ordrePcf.setValue(0.05f);
                }
            }
            else if (Eb < 0.55 && Eb > 0.45) {
                if (Pw + Ps <= 1000){

                    ordrePf.setValue(0.5f);
                ordrePcf.setValue(0.1f);
                }
                else {
                    ordrePf.setValue(0.7f);
                    ordrePcf.setValue(0);
                }
            }
            else if(Ph+Pb+Pp>Pw+Ps && Eb < 0.4){

                ordrePf.setValue(0);
                ordrePcf.setValue(((Ph+Pb+Pp-Pw-Ps)/500)+0.25f);
            }
            else {

                ordrePf.setValue(1);
                ordrePcf.setValue(0);
            }

        } else {
            if (Pw + Ps > 1000 && Eb > 0.9) {
                ordreS.setValue(false);
                ordrePf.setValue(Pw/1000);
            }
            else if (Pw + Ps > 1000 && Eb < 0.9 && !SolarOnOff) {
                ordreS.setValue(true);
                ordrePf.setValue(1);
            }
            else if (0.55 > Eb && Eb > 0.5) {
                    ordrePcf.setValue(0.2f);
                    if (Pw + Ps < 1000) ordrePf.setValue(0.6f);
                    else ordrePf.setValue(1);
                }
            else if (Eb < 0.5) {
                    ordrePcf.setValue(1);
                    if (Pw + Ps < 1000) ordrePf.setValue(0.2f);
                    else ordrePf.setValue(1);
                }
            else if (Eb > 0.55 && Pcf > 0) {
                    ordrePcf.setValue(0);
                    ordrePf.setValue(1);
                }
            else ordrePf.setValue(1);
        }
    }


    /**
     * Schedules a TimerTask to run the 'regulation' method at a fixed rate.
     *
     * @param periode The period of time between each execution of the TimerTask, in milliseconds.
     */
    public static void timerTask(int periode) {
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                regulation();
            }
        }, 0, periode);

    }

}
