package ch.hevs.isi.db;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/**
 A singleton class that connects to a database and pushes data points to it.
 The DatabaseConnector class implements the DataPointListener interface to handle new data points received from the WebConnector

 It sends data to the specified database server over HTTP.
 */
public class DatabaseConnector implements DataPointListener {
    /**

     The only instance of DatabaseConnector
     */
    private static DatabaseConnector instance = null;
    /**

     The base URL for the database server
     */
    private URL url = null;
    /**

     The token to authenticate with the database server
     */
    private String token = null;


    public TimeManager get_timeManager() {
        return _timeManager;
    }
    /**

     The time manager used to manage timestamps
     */
    private TimeManager _timeManager = new TimeManager(3);
    /**

     The last timestamp used for a data point
     */
    private long timeStamp=0;

    /**

     Constructor for DatabaseConnector, private to ensure singleton pattern
     */
    private DatabaseConnector() {
    }

    /**

     Returns the only instance of DatabaseConnector, creates it if it doesn't exist
     @return The instance of DatabaseConnector
     */
    public static DatabaseConnector getInstance() {
        if (instance == null)
            instance = new DatabaseConnector();
        return instance;
    }

    /**

     Sends data to the database server over HTTP

     @param label The identifier of the datapoint

     @param value The value of the datapoint
     */
    private void pushToDatabase(String label, String value) {

        try {
            if (url != null) {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Authorization", "Token " + token);
                connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);

                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());

                String solution2 = "minecraft " + label + "=" + value + " " + timeStamp;
                out.write(solution2);
                out.flush();
                out.close();

                int responseCode = connection.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_NO_CONTENT) {
                    System.out.println("Oups !");
                }

                connection.disconnect();
            } else
                System.out.println("No URL, please configure");
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    /**

     Called when a new data point is received from WebConnector

     @param dp The data point received
     */
    public void onNewValue(DataPoint dp) {

        if (dp.getLabel().equals("CLOCK_FLOAT")) {
            _timeManager.setTimestamp(dp.getValue());
            timeStamp = _timeManager.getNanosForDB();
        }
        if ( timeStamp != 0) {
           pushToDatabase(dp.getLabel(), dp.getValue());
        }
    }

    /**

     Configures the database connector with the specified URL, organization, bucket, and token
     @param baseUrl The base URL of the database server
     @param organisation The name of the organization on the database server
     @param bucket The name of the bucket on the database server
     @param token The authentication token for the database server
     */
    public void configure(String baseUrl, String organisation, String bucket, String token) throws MalformedURLException {
        url = new URL(baseUrl + "/api/v2/write?org=" + organisation + "&bucket=" + bucket);
        this.token = token;
    }

    /**
     *
     * @param baseUrl The base URL of the database server
     * @param organisation The name of the organization on the database server
     * @param bucket The name of the bucket on the database server
     * @param token The authentication token for the database server
     * @param actualTime the time at the moment the program is launched
     * @throws MalformedURLException
     * Do {@link #configure(String, String, String, String)} at the end of the function
     */
    public void clearDataBase(String baseUrl, String organisation, String bucket, String token,String actualTime) throws MalformedURLException {
        try {

            url = new URL(baseUrl + "/api/v2/delete?org=" + organisation + "&bucket=" + bucket);
            this.token = token;
            HttpURLConnection connection = null;
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Authorization", "Token " + token);
            connection.setRequestProperty("Content-Type", "text/plain; charset=utf-8");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);

            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            String messageJson = "{\n"+
                    "\"start\": \"2000-01-01T00:00:00Z\",\n"+
                    "\"stop\": \""+actualTime+"\",\n"+
                    "\"predicate\": \"_measurement=\\\"Minecraft\\\"\"\n"+
                    "}";

            out.write(messageJson);
            out.flush();
            out.close();

            int responseCode = connection.getResponseCode();

            if (responseCode != HttpURLConnection.HTTP_NO_CONTENT) {
                System.out.println("URL NOT FOUND");
            }

            connection.disconnect();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        DatabaseConnector.getInstance().configure(baseUrl,organisation,bucket,token);
    }


}
