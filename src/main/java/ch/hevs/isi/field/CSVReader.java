package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Vector;

/**
 * This class as the function to reed the CSV for assigning all the datapoint
 */
/**
 *The CSVReader class reads a CSV file named "ModbusMap.csv" and creates Register objects with the values
 *from the file. The class contains a static method called assignDataPoint() which assigns the data points
 *read from the CSV file to the corresponding Register objects.
 */
public class CSVReader {


    /**
     *This method reads the CSV file and assigns the data points to Register objects.
     *It creates a BufferedReader object to read the file, and a Vector object to store
     *each line of the file as a string. It then removes the first line and all empty lines
     *from the Vector object, and creates Register objects with the values from the CSV file.
     */
    public static void assignDataPoint() {
        BufferedReader br = Utility.fileParser(null, "ModbusMap.csv");
        Vector<String> lines = new Vector<>();
        try {
            //read the file
            do {
                lines.add(br.readLine());

            } while (lines.lastElement() != null);
            //remove first line and all empty lines
            lines.removeElement(null);
            lines.removeElementAt(0);

            //create Register with the value of the CSV inside
            for (String line : lines) {
                String[] params = line.split(";");
                boolean isOutput = params[3].equals("Y");
                //control if boolean or float register, for using the adapted constructor
                if (params[1].equals("B")) {
                    new BooleanRegister(params[0], isOutput, Integer.parseInt(params[4]));
                } else {
                    new FloatRegister(params[0], isOutput, Integer.parseInt(params[4]), Integer.parseInt(params[5]), Integer.parseInt(params[6]));
                }
            }

        } catch (IOException e) {
            System.err.println(e.getStackTrace());


        }


    }

}
