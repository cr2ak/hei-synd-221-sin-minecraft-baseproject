package ch.hevs.isi.field;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.ip.tcp.TcpMaster;
import com.serotonin.modbus4j.locator.BaseLocator;
/**

 The ModbusAccessor class provides methods to read and write data from a Modbus server using the TCP protocol.
 */
public class ModbusAccessor {
    /**

     The TcpMaster instance used to communicate with the Modbus server.
     */
    private TcpMaster master = null;
    /**

     The unique instance of the ModbusAccessor class.
     */
    private static ModbusAccessor ma = null;
    /**

     The IP parameters used to connect to the Modbus server.
     */
    private IpParameters param =null;
    /**

     Constructs a new ModbusAccessor object.
     This constructor is private because this class is a singleton and cannot be instantiated from outside.
     */
    private ModbusAccessor() {
    }

    /**

     Returns the unique instance of the ModbusAccessor class.
     If the instance does not exist, creates a new one.
     @return the ModbusAccessor instance.
     */
    public static ModbusAccessor getInstance() {
        if (ma == null) {
            ma = new ModbusAccessor();
        }
        return ma;

    }
    /**

     Reads a float value from the Modbus server.
     @param regAdress the register address to read the float value from.
     @return the float value read from the Modbus server.
     @throws RuntimeException if an error occurs while reading the float value from the Modbus server.
     */
    public float readFloat(int regAdress) {
        try {
            return (float) master.getValue(BaseLocator.inputRegister(1, regAdress, DataType.FOUR_BYTE_FLOAT));
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }

    }
    /**

     Writes a float value to the Modbus server.
     @param regAdress the register address to write the float value to.
     @param newValue the new float value to write to the Modbus server.
     @throws RuntimeException if an error occurs while writing the float value to the Modbus server.
     */
    public void writeFloat(int regAdress, float newValue) {
        try {
            master.setValue(BaseLocator.holdingRegister(1, regAdress, DataType.FOUR_BYTE_FLOAT), newValue);
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }
    /**

     Reads a boolean value from the Modbus server.
     @param regAdress the register address to read the boolean value from.
     @return the boolean value read from the Modbus server.
     @throws RuntimeException if an error occurs while reading the boolean value from the Modbus server.
     */
    public boolean readBoolean(int regAdress) {
        try {
            return master.getValue(BaseLocator.coilStatus(1, regAdress));
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }
    /**

     Writes a boolean value to the Modbus server.
     @param regAdress the register address to write the boolean value to.
     @param newValue the new boolean value to write to the Modbus server.
     @throws RuntimeException if an error occurs while writing the boolean value to the Modbus server.
     */
    public void writeBoolean(int regAdress, boolean newValue) {
        try {
            master.setValue(BaseLocator.coilStatus(1, regAdress), newValue);
        } catch (ModbusTransportException e) {
            throw new RuntimeException(e);
        } catch (ErrorResponseException e) {
            throw new RuntimeException(e);
        }
    }

    /**

     Attempts to connect to the Modbus server using the default IP address and port number.
     @return true if the connection was successful, false otherwise
     */
    public boolean connect() {

        if (param == null)
            return false;

        master = (TcpMaster) new ModbusFactory().createTcpMaster(param, true);

        try {
            master.init();
            return true;
        } catch (ModbusInitException e) {
            return false;
        }
    }



    /**

     Connect to a server using the specified IP address and port.
     @param ipAdress the IP address of the server.
     @param port the port number of the server.
     @return true if the connection was successful, false otherwise.
     */
    public boolean connect(String ipAdress, int port) {
        param = new IpParameters();
        param.setHost(ipAdress);
        param.setPort(port);
        return connect();
    }


    public static void main(String[] args) {
        ModbusAccessor m = getInstance();
        if(m.connect())
            System.out.println("You are connected");
        else
            System.err.println("Something wrong");

        if(m.connect("localhost", 1502)) {
            System.out.println("You are connected");

        }else
            System.err.println("Something wrong");

        System.out.println(ma.readBoolean(609));

    }



}