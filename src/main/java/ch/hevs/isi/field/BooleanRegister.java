package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;

import java.util.HashMap;
/**

 The BooleanRegister class extends the ModbusRegister class and represents a register that contains a boolean value.

 It contains a BooleanDataPoint object to store the label and the output status of the register, and an integer

 address that represents the address of the register. The class contains a read() method that reads the boolean

 value from the Modbus server and assigns it to the BooleanDataPoint object, and a write() method that writes the

 boolean value to the Modbus server.
 */
public class BooleanRegister extends ModbusRegister {
    /**
     * Attributes
     *
     */
    private BooleanDataPoint dp;

    private static ModbusAccessor ma = ModbusAccessor.getInstance();
    /**

     Constructs a BooleanRegister object with the given label, output status, and address. The BooleanDataPoint
     object is initialized with the label and output status, and the object is added to the hashmap hm with
     the BooleanDataPoint object as the key and the BooleanRegister object as the value.
     @param label the label of the register
     @param isOutput true if the register is an output, false otherwise
     @param address the address of the register
     */
    BooleanRegister(String label, boolean isOutput, int address) {
        this.address = address;
        this.dp = new BooleanDataPoint(label, isOutput);
        hm.put(dp, this);

    }

    /**

     Reads the boolean value from the Modbus server and assigns it to the BooleanDataPoint object.
     */

    public void read() {

        dp.setValue(ma.readBoolean(address));

    }

    /**
     * Put a new Value on the ModbusAccessor
     */

    public void write() {
        ma.writeBoolean(address, dp.getBooleanValue());
    }

}

