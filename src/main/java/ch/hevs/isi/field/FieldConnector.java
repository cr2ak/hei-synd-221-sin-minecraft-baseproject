package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.db.DatabaseConnector;

import java.util.Timer;
import java.util.TimerTask;


/**

 The FieldConnector class connects data points to Modbus registers and schedules polling tasks.

 It implements the DataPointListener interface and uses a singleton pattern to ensure only one instance exists.
 */
public class FieldConnector implements DataPointListener {

    private static FieldConnector instance = null;

    /**

     Constructor for FieldConnector class.
     It does nothing.
     */
    private FieldConnector() {

    }
    /**

     Returns the only instance of FieldConnector.
     @return the instance of FieldConnector
     */
    public static FieldConnector getInstance() {
        if (instance == null) {
            instance = new FieldConnector();
        }
        return instance;
    }
    /**

     Sends the transmitted data to the associated Modbus register.
     @param dp the transmitted data
     */
    private void pushToField(DataPoint dp) {
        ModbusRegister.getRegisterFromDataPoint(dp).write();


    }
    /**

     Handles the receipt of new data from a data point by pushing it to the associated Modbus register.
     @param dp the new data point
     */
    public void onNewValue(DataPoint dp) {

        pushToField(dp);

    }
    /**

     Schedules a timer to repeatedly poll Modbus registers at the specified period.
     @param periode the time period between consecutive pollings
     */
    public void pollTask(int periode){
        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ModbusRegister.poll();
            }
        }, 0, periode);
    }

}
