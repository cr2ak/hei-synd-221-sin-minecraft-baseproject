package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;

import java.util.HashMap;
import java.util.Map;
/**

 The abstract class {@code ModbusRegister} represents a Modbus register.

 Each register is associated with an address and contains a data point map.

 It provides the basic methods to read and write register data, as well as to retrieve

 the register associated with a specific data point. It also contains a polling method to

 read data from all registers.
 */
public abstract class ModbusRegister {
    /** The Modbus address associated with this register */
    protected int address;
    /** A mapping of data points to their associated registers */
    protected static Map<DataPoint, ModbusRegister> hm = new HashMap<>();
    /**

     Retrieves the register associated with the specified data point
     @param dp the data point to retrieve the register for
     @return the register associated with the specified data point
     */
    public static ModbusRegister getRegisterFromDataPoint(DataPoint dp) {
        return hm.get(dp);
    }
    /**

     Reads the register data
     */
    public abstract void read();
    /**

     Writes data to the register
     */
    public abstract void write();
    /**

     Polls all registers to read their data
     */
    public static void poll() {

        for (ModbusRegister br : hm.values()) {

            br.read();
        }
    }
}
