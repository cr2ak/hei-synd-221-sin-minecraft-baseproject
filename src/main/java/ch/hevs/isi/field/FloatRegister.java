package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.HashMap;
/**

 The FloatRegister class extends the ModbusRegister class and represents a register that contains a floating point value.

 It contains a FloatDataPoint object to store the label and the output status of the register, an integer address that

 represents the address of the register, and two integers range and offset that represent the range and offset of the

 floating point value. The class contains a read() method that reads the floating point value from the Modbus server,

 scales it by the range and offset values, and assigns it to the FloatDataPoint object, and a write() method that writes

 the scaled floating point value to the Modbus server.
 */
public class FloatRegister extends ModbusRegister {
    /**
     * declaration des variables
     */
    private static ModbusAccessor ma = ModbusAccessor.getInstance();
    private FloatDataPoint dp;
   private int range;
    private int offset;
    /**

     Constructs a FloatRegister object with the given label, output status, address, range, and offset values.
     The FloatDataPoint object is initialized with the label and output status, and the object is added to the
     hashmap hm with the FloatDataPoint object as the key and the FloatRegister object as the value.
     @param label the label of the register
     @param isOutput true if the register is an output, false otherwise
     @param address the address of the register
     @param range the range of the floating point value
     @param offset the offset of the floating point value
     */
    FloatRegister(String label, boolean isOutput, int address, int range, int offset){
        this.dp = new FloatDataPoint(label, isOutput);
        this.address = address;
        this.offset= offset;
        this.range=range;
        hm.put(dp, this);
    }

    /**

     Reads the floating point value from the Modbus server, scales it by the range and offset values, and assigns it
     to the FloatDataPoint object.
     */
    public void read() {
        dp.setValue(ma.readFloat(address)*range+offset);
    }

    /**

     Writes the scaled floating point value to the Modbus server.
     */
    public void write(){
        ma.writeFloat(address, (dp.getFloatValue()-offset)/range);
    }



}
