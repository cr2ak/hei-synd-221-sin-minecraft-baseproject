package ch.hevs.isi.web;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;



/**

 The WebConnector class represents a websocket server that broadcasts messages to clients and receives messages from them.

 It implements the DataPointListener interface in order to receive notifications of new values for data points.
 */
public class WebConnector implements DataPointListener {
    /**

     The instance of the WebConnector.
     */
    private static WebConnector instance = null;
    /**

     The WebSocketServer used for handling WebSocket connections.
     */
    private WebSocketServer wss = null;
/**

 Constructor. Creates a new websocket server that listens on port 8888 and broadcasts messages to clients.

 Sets up the websocket server with onOpen, onClose, onMessage, onError and onStart methods.
 */


    private WebConnector() {
        wss = new WebSocketServer(new InetSocketAddress(8888)) {
            /**
             * Callback function that is executed when a new client connection is opened.
             * @param webSocket The WebSocket instance for the client connection.
             * @param clientHandshake The handshake for the client connection.
             */
            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                webSocket.send("Welcome to Minecraft World");
            }
            /**
             * Callback function that is executed when a client connection is closed.
             * @param webSocket The WebSocket instance for the client connection.
             * @param i The status code for the connection close.
             * @param s The reason for the connection close.
             * @param b Boolean indicating whether or not the connection was closed cleanly.
             */
            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {

            }
            /**
             * Callback function that is executed when a new message is received from a client connection.
             * @param webSocket The WebSocket instance for the client connection.
             * @param s The message received from the client connection.
             */
            @Override
            public void onMessage(WebSocket webSocket, String s) {
                //separate the label and the value
                String[] params = s.split("=");
                //found the datapoint from the label
                DataPoint dp = DataPoint.getDataPointFromLabel(params[0]);

                //set the Value on the adapted datapoint
                if (dp.getClass() == FloatDataPoint.class) {
                    dp.setValue(Float.valueOf(params[1]));
                } else {
                    dp.setValue(Boolean.parseBoolean(params[1].toLowerCase()));
                }
            }
            /**
             * Callback function that is executed when an error occurs on a client connection.
             * @param webSocket The WebSocket instance for the client connection.
             * @param e The Exception that was thrown.
             */
            @Override
            public void onError(WebSocket webSocket, Exception e) {

            }
            /**
             * Callback function that is executed when the WebSocketServer is started.
             */
            @Override
            public void onStart() {

            }

        };


        wss.start();

    }
    /**

     Get the instance of the WebConnector.

     If it does not exist, create it.

     @return The instance of the WebConnector.
     */

    public static WebConnector getInstance() {
        //if not exist, create an instance
        if (instance == null)
            instance = new WebConnector();

        return instance;
    }

    /**

     Broadcasts a message with the label and value of a data point to all connected clients.
     @param label String identifier of a data point
     @param value value of the transmitted data, can be boolean or float
     */
    private void pushToWeb(String label, String value) {
        String message = label+"="+value;
        for (WebSocket ws : wss.getConnections()) {
            ws.send(message);
        }

    }

    /**

     Implementation of the onNewValue method of the DataPointListener interface.
     Receives notifications of new values for data points and broadcasts them to all connected clients.
     @param dp transmitted data
     */
    public void onNewValue(DataPoint dp) {
        pushToWeb(dp.getLabel(), dp.getValue());
    }

}
