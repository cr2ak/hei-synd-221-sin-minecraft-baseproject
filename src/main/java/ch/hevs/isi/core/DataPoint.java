package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

import java.util.HashMap;

/**
 * Describe the transmitted data between the minecraft World and the web
 */
public abstract class DataPoint {
    /**
     * Attributes
     */
    private final String label;
    private final boolean isOutput;
    private static final HashMap<String, DataPoint> hm = new HashMap<>();

    /**
     * Constructor
     *
     * @param label    String identifier of a datapoint
     * @param isOutput true if output(control), false if input(measurement)
     */
    DataPoint(String label, boolean isOutput) {
        this.label = label;
        this.isOutput = isOutput;
        hm.put(label, this);

    }

    /**
     * @param label String identifier of a DataPoint
     * @return Datapoint corresponding to the label
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return hm.get(label);
    }

    /**
     *
     * @return String identifier of the DataPoint
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @return true if the Datapoint is an output(control), false if is an input(measurement)
     */
    public boolean isOutput() {
        return isOutput;
    }


    /**
     * Function to set a value in the Datapoint
     * is implemented in the daughters class
     */
   public void setValue(boolean value) {
    }
    /**
     * Function to set a value in the Datapoint
     * is implemented in the daughters class
     */
    public void setValue(float value) {
    }
    /**
     *
     * is implemented in the daughters class
     * @return the value from the Datapoint
     */
    public abstract String getValue();

    /**
    * Update the Connectors
     */
    protected void pushValueToConnectors() {

        DatabaseConnector.getInstance().onNewValue(this);
        WebConnector.getInstance().onNewValue(this);
        if (isOutput) {
            FieldConnector.getInstance().onNewValue(this);
        }
    }

    /**
    *Test of functionality
     */
    public static void main(String[] args) {
        FloatDataPoint a = new FloatDataPoint("Jean", true);
        a.setValue(1.78f);
        BooleanDataPoint b = new BooleanDataPoint("Pierre", false);
        b.setValue(true);
    }

}
