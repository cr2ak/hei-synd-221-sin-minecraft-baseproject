package ch.hevs.isi.core;

/**
 * Interface Datapoint listener
 */

public interface DataPointListener {


    /**
     *
     * @param dp the Datapoint
     */

    void onNewValue(DataPoint dp) ;


}
