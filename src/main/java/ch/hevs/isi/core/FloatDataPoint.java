package ch.hevs.isi.core;

/**
 * Describe the float message between the Minecraft world and the web
 */
public class FloatDataPoint extends DataPoint {
    /**
     * Attributes
     */
    private float value;

    /**
     * Constructor
     *
     * @param label    String identifier of a datapoint
     * @param isOutput true if output(control), false if input(measurement)
     */
   public FloatDataPoint(String label, boolean isOutput) {
        super(label, isOutput);

    }


    /**
     *
     * @param value float value in the
     */
    @Override
    public void setValue(float value) {
        this.value = value;

        pushValueToConnectors();
    }

    /**
     *
     * @return the float value converted in string
     */
    public String getValue() {
        return String.valueOf(value);

    }

    /**
     *
     * @return the float value
     */
    public float getFloatValue(){ return value;}

}
