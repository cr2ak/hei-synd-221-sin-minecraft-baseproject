package ch.hevs.isi.core;

/**
 * Describe the boolean message between the Minecraft world and the web
 */
public class BooleanDataPoint extends DataPoint {
    /**
     * value of the datapoint
     */
    private boolean value;

    /**
     * @param label    String identifier of a datapoint
     * @param isOutput true if output(control), false if input(measurement)
     */
   public BooleanDataPoint(String label, boolean isOutput) {
        super(label, isOutput);

    }

    /**
     *
     * @param value boolean to set in the DataPoint
     */
    @Override
    public void setValue(boolean value) {
        this.value = value;
        pushValueToConnectors();
    }

    /**
     *
     * @return the boolean value converted in string
     */
    public String getValue() {
        return String.valueOf(value);
    }

    /**
     *
     * @return the boolean value
     */
    public boolean getBooleanValue() { return value; }
}
